const express = require('express')
const app = express()
const path =require("path")
const port = 3000
const methodOverride = require('method-override')

app.use(express.urlencoded({extended:true}))
app.use(express.json())

app.use(express.static(path.join(`${__dirname}`,"public")))

app.use(methodOverride("_method"))

app.set("view engine","ejs")
app.set("views",path.join(__dirname,"views"))

app.get('/', (req, res) => res.send('Hello World!'))


// database is not there I use the object array
let posts=[
    {
        id : 1,
        username:"Nikhil",
        content : "abcdefghi"
    },
    {
        id : 2,
        username:"Kumar",
        content : "1abcdefghi"
    },
    {
        id : 3,
        username:"Lucky",
        content : "2abcdefghi"
    },
]
// To get all the posts
app.get ('/posts',(req,res)=>{
  res.render("posts.ejs",{posts})
})
// to add a new post form will be display
app.get('/posts/newPost',(req,res)=>{
    res.render("addPosts.ejs")
})

// "post" request will update the post / add the new post 
// for doing the add post in the form action = "posts" will be there
app.post ('/posts',(req,res)=>{
   // console.log(req.body)
   const data =req.body
    const newId = posts.length + 1
    const newData = Object.assign({id : newId},data)
    //console.log(newId)
  //  console.log(newData)
    posts.push(newData)
   // res.send("Added the new Posts")
   res.redirect("/posts")
})

// get the post by id
app.get('/posts/:id',(req,res)=>{
    let {id} = req.params
   // console.log(id)
   const singleData = posts.find(ele => ele.id == id ) 
  //  console.log(singleData)
  res.render("seeDetails.ejs",{singleData})
})
// use to update the content
app.patch('/posts/:id',(req,res)=>{
    let {id}=req.params
   // console.log(id)
    const newContent = req.body.content
  //  console.log("New contenet",newContent)
    let post = posts.find(ele => ele.id == id)
    post.content=newContent 
   // console.log("postss",post)
    res.redirect("/posts")
})

//use to get the edit/update the "display content" form
app.get('/posts/:id/edit',(req,res)=>{
    let {id}= req.params
    let post =posts.find(ele => ele.id == id)
    res.render("edit.ejs",{post}) 
})


app.delete('/posts/:id/',(req,res)=>{
    let {id}=req.params
   // console.log(id)
    posts =posts.filter(ele => ele.id != id)
   res.redirect("/posts")
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`)) 